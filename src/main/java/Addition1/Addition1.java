package Addition1;


public class Addition1 {
    private int somme;

    public int getSomme() {
        return somme;
    }

    public void setSomme(int somme) {
        this.somme = somme;
    }

    private int ch1;

    public int getCh1() {
        return ch1;
    }

    public void setCh1(int ch1) {
        this.ch1 = ch1;
    }
    
    private int ch2;

    public int getCh2() {
        return ch2;
    }

    public void setCh2(int ch2) {
        this.ch2 = ch2;
    }
    
public void calcul() {
    somme = ch1 + ch2;
}

public static void main(String[] args) {
       
        Addition1 xy = new Addition1();
        xy.setCh1(5);
        xy.setCh2(10);
        xy.calcul();
        System.out.println("Le r�sultat est "+ xy.getSomme());
    }
}
